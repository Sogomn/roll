use std::{
	env,
};

struct Die {
	d: u32,
}

struct Constant {
	v: u32,
}

struct Roll {
	subrolls: Vec<Box<dyn ToValue>>,
}

trait ToValue {
	fn value(&self) -> u32;
}

impl Die {
	fn new(d: u32) -> Self {
		Self {
			d: d,
		}
	}
	
	fn roll(&self) -> u32 {
		if self.d == 0 {
			return 0;
		}
		
		let random: u32 = rand::random();
		let roll = (random % self.d).checked_add(1).unwrap_or(self.d);
		
		roll
	}
}

impl ToValue for Die {
	fn value(&self) -> u32 {
		self.roll()
	}
}

impl Constant {
	fn new(v: u32) -> Self {
		Self {
			v: v,
		}
	}
}

impl ToValue for Constant {
	fn value(&self) -> u32 {
		self.v
	}
}

impl Roll {
	fn new() -> Self {
		Self {
			subrolls: Vec::new(),
		}
	}
	
	fn add_subroll(&mut self, r: Box<dyn ToValue>) {
		self.subrolls.push(r);
	}
	
	fn roll(&self) -> u32 {
		let sum = self.subrolls.iter()
			.map(Box::as_ref)
			.map(ToValue::value)
			.try_fold(0u32, |acc, val| {
				acc.checked_add(val)
			});
		
		sum.unwrap_or(0)
	}
}

impl ToValue for Roll {
	fn value(&self) -> u32 {
		self.roll()
	}
}

fn parse_argument(s: impl AsRef<str>) -> Box<dyn ToValue> {
	let mut s = s.as_ref();
	
	if s.starts_with("D") || s.starts_with("d") {
		s = s.split_at(1).1;
		
		let val = s.parse().unwrap_or(0);
		
		Box::new(Die::new(val))
	} else {
		let val = s.parse().unwrap_or(0);
		
		Box::new(Constant::new(val))
	}
}

fn main() {
	let args: Vec<String> = env::args().skip(1).collect();
	let mut roll = Roll::new();
	
	for arg in args.iter() {
		let parsed = parse_argument(arg);
		
		roll.add_subroll(parsed);
	}
	
	let result = roll.roll();
	
	println!("{}", result);
}

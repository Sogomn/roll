# Roll (dice)

## Usage

```
roll d20 10 d6 3
```

Rolls a D20, a D6 and adds 13 to the result.
